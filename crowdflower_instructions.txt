<h1>Overview</h1>

<p>In this task, you will need to decide whether a text <em>a)</em> represents a data triple, <em>b)</em> is good English, <em>c)</em> is fluent.</p>
<hr>

<h1>Steps</h1>

<ul>
	<li><strong>Review</strong> the data and text.</li>
	<li><strong>Check</strong>whether the text has <strong>all and only the information</strong>from the data.</li>
	<li><strong>Check</strong>whether the text is grammatical and whether the spelling is correct.</li>
	<li><strong>Check</strong> whether the text is fluent and natural.</li>
</ul>
<hr>

<h1>Rules Tips</h1>

<ul>
	<li>The text must express <strong>all</strong> the data.</li>
	<li>The text must express the data correctly.</li>
	<li>The text should be grammatical (no spelling or grammatical errors).</li>
	<li>The text should sound fluent and natural.</li>
	<li>All texts are in small letters, so ignore spelling mistakes with capitalisations.</li>
</ul>
<hr>

<h1>Examples</h1>

<p><strong>Data:</strong></p>

<p>Elliot\_See | occupation | Test\_pilot
	<br>Elliot\_See | birthDate | "1927-07-23"</p>

<p><strong>Text:</strong></p>

<p><em>elliot see flew as a test pilot, and elliot see were born on 23rd june 1927.</em></p>

<p>Does the text correctly represent the meaning from the data?
	<br><strong>Incorrectly.</strong> (Elliot See was born in July, not June.)</p>

<p>Rate the grammar and the spelling of the text:
	<br><strong>Ungrammatical.</strong>("were" must be "was")</p>

<p>Rate the fluency of the text:
	<br><strong>Not fluent/Medium.</strong> ("Elliot See" is repeated twice.)</p>

<p>
	<br>
</p>

<p><strong>Data:</strong></p>

<p>Aleksandra\_Kovač | background | "solo\_singer"</p>

<p><strong>Text:</strong></p>

<p><em>alexanandra kovair is the full name of the full name of alexanandra kovair.</em></p>

<p>Does the text correctly represent the meaning from the data?
	<br><strong>Incorrectly.</strong> (The text doesn't mention that she is a solo singer.)</p>

<p>Rate the grammar and the spelling of the text:
	<br><strong>Ungrammatical.</strong>(Spelling mistakes. "<em>alexanandra kovair</em>" should be "<em>aleksandra kovac</em>".)</p>

<p>Rate the fluency of the text:
	<br><strong>Not fluent/Medium.</strong> ("<em>full name</em>" is repeated twice.)</p>
<hr>

