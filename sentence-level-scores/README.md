# Sentence-level scores

METEOR and TER sentence-level scores were calculated by the official challenge evaluation scripts described here: <https://webnlg-challenge.loria.fr/challenge_2017/#evaluating-on-a-development-set>

BLEU sentence-level scores were calculated with NLTK.

Example:

```python
import nltk


hyp = 'the 29075 club is the dictcoverer, carl a. wirtsen.'
refs = ['( 29075 ) 1950 da was discovered by carl a wirtanen .',
        'carl a wirtanen discovered ( 29075 ) 1950 da .',
        'carl a wirtanen was the discoverer of 29075 1950 da .']

hyp_tokenised = hyp.split()
refs_tokenised = [ref.split() for ref in refs]

chencherry = nltk.translate.bleu_score.SmoothingFunction()

bleu_sent = nltk.translate.bleu_score.sentence_bleu(refs_tokenised,
    hyp_tokenised,
    smoothing_function=chencherry.method3,
    weights=(0.25, 0.25, 0.25, 0.25))

```
