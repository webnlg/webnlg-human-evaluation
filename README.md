### WebNLG Challenge 2017 Human Evaluation
The repository contains files used for human evaluation in the [WebNLG Challenge 2017](https://webnlg-challenge.loria.fr/challenge_2017/#challenge-results).

More details can be found in the [report](https://hal.archives-ouvertes.fr/hal-03007072).

We sampled 223 (data, text) pairs for human evaluation.

* _sample-ids.txt_ contains sampled ids from test set
* _MRs.txt_ contains data
* _gold-sample-reference*.lex_ contains references
* _all\_data\_final\_averaged.csv_ contains human evaluation results. Fluency, Semantics, and Grammar columns represent average scores that we got by collecting three human judgments. References are denoted by _webnlg_; their automatic scores are always 1.0 or 0.0.
* _all\_data\_final\_scores\_anonymised.csv_ contains raw human scores. 

To replicate the results, run `human-evaluation.R`.

Results obtained in this study also gave rise to the following publication:

Anastasia Shimorina. _Human vs Automatic Metrics: on the Importance of Correlation Design_. WiNLP Workshop at NAACL, 2018. [[pdf](https://arxiv.org/abs/1805.11474)] [[poster](winlp18_poster.pdf)]

### Citing

```
@techreport{shimorina-2020-webnlg-human-eval,
  TITLE = {{WebNLG Challenge: Human Evaluation Results}},
  AUTHOR = {Shimorina, Anastasia and Gardent, Claire and Narayan, Shashi and Perez-Beltrachini, Laura},
  URL = {https://hal.archives-ouvertes.fr/hal-03007072},
  TYPE = {Technical Report},
  INSTITUTION = {{Loria \& Inria Grand Est}},
  YEAR = {2018},
  MONTH = Jan,
  PDF = {https://hal.archives-ouvertes.fr/hal-03007072v2/file/human-eval-outline-v3.pdf},
  HAL_ID = {hal-03007072},
  HAL_VERSION = {v3},
}
```

